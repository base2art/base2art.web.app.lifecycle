namespace Base2art.Web.App.LifeCycle
{
    using System.Threading.Tasks;

    public interface IApplication
    {
        void Reload();

        Task ExecuteJob(string name);

        Task ExecuteJobWithParameters(string name, object parameters);
    }
}